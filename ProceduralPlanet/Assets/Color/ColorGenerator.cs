using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ColorSettings.BiomeColorSettings;

public class ColorGenerator
{
    #region properties
    ColorSettings colorSettings;
    Texture2D texture;
    const int textureResolution = 50;
    INoiseFilter biomeNoiseFilter;
    #endregion

    #region Constructor
    public void UpdateSettings(ColorSettings colorSettings)
    {
        this.colorSettings = colorSettings;
        if (texture == null || texture.height != colorSettings.biomeColorSettings.biomes.Length)
            texture = new Texture2D(textureResolution * 2, colorSettings.biomeColorSettings.biomes.Length, TextureFormat.RGBA32, false);
        biomeNoiseFilter = NoiseFilterFactory.CreateNoiseFilter(colorSettings.biomeColorSettings.noise);
    }
    #endregion

    #region Method
    public void UpdateElevation(MinMax elevationMinMax)
    {
        colorSettings.planetMaterial.SetVector("_elevationMinMax", new Vector4(elevationMinMax.Min, elevationMinMax.Max));
    }
    public float BiomePercentFromPoint(Vector3 pointOnUnitSphere)
    {
        float heightPercent = (pointOnUnitSphere.y + 1) / 2f;
        heightPercent += (biomeNoiseFilter.Evaluate(pointOnUnitSphere) - colorSettings.biomeColorSettings.noiseOffset) * colorSettings.biomeColorSettings.noiseStrenght;

        float blendRange = colorSettings.biomeColorSettings.blendAmount / 2f + 0.001f;
        float biomeIndex = 0;
        int numbBiome = colorSettings.biomeColorSettings.biomes.Length;
        for (int i = 0; i < numbBiome; i++)
        {
            float distance = heightPercent - colorSettings.biomeColorSettings.biomes[i].startHeight;
            float weight = Mathf.InverseLerp(-blendRange, blendRange, distance);
            biomeIndex *= (1 - weight);
            biomeIndex += i * weight;
        }
        return biomeIndex / Mathf.Max(1, numbBiome - 1);
    }
    public void UpdateColors()
    {
        Color[] colors = new Color[texture.width * texture.height];
        int colorIndex = 0;
        foreach (Biome biome in colorSettings.biomeColorSettings.biomes)
        {
            for (int i = 0; i < textureResolution * 2; i++)
            {
                Color gradientColor;
                if (i < textureResolution)                
                    gradientColor = colorSettings.oceanColor.Evaluate(i / (textureResolution - 1f));                
                else                
                    gradientColor = biome.gradient.Evaluate((i - textureResolution) / (textureResolution - 1f));
                
                Color tintColor = biome.tint;
                colors[colorIndex] = gradientColor * (1 - biome.tintPercent) + tintColor * biome.tintPercent;
                colorIndex++;
            }
        }

        texture.SetPixels(colors);
        texture.Apply();
        colorSettings.planetMaterial.SetTexture("_texture", texture);
    }
    #endregion
}

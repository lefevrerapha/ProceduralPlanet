using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static NoiseSettings;

public class SimpleNoiseFilter : INoiseFilter
{
    #region properties
    Noise noise = new Noise();
    NoiseSettings.SimpleNoiseSettings noiseSettings;
    #endregion

    #region Construcotr
    public SimpleNoiseFilter(SimpleNoiseSettings noiseSettings)
    {
        this.noiseSettings = noiseSettings;
    }
    #endregion

    #region Method
    public float Evaluate(Vector3 point)
    {
        float noiseValue = 0;
        float frequency = noiseSettings.baseRoughness;
        float amplitude = 1;

        for (int i = 0; i < noiseSettings.numberLayer; i++)
        {
            float v = noise.Evaluate(point * frequency + noiseSettings.centre);
            noiseValue += (v + 1) * 0.5f * amplitude;

            frequency *= noiseSettings.roughness;
            amplitude *= noiseSettings.persistence;
        }
        noiseValue = noiseValue - noiseSettings.minValue;
        return noiseValue * noiseSettings.strength;
    }
    #endregion
}

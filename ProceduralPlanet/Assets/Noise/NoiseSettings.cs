using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NoiseSettings
{
    public enum FilterType { Simple, Rigide };
    public FilterType filterType;
    [ConditionalHide("filterType", 0)]
    public SimpleNoiseSettings simpleNoiseSettings;
    [ConditionalHide("filterType", 1)]
    public RigideNoiseSettings rigideNoiseSettings;

    [Serializable]
    public class SimpleNoiseSettings
    {
        public float strength = 1;
        public float baseRoughness = 1;
        public float roughness = 2;
        public float persistence = 0.5f;
        public Vector3 centre;

        [Range(1, 8)]
        public int numberLayer = 3;
        public float minValue;
    }

    [Serializable]
    public class RigideNoiseSettings : SimpleNoiseSettings
    {
        public float weightMultiplier = 0.8f;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    #region properties
    [Range(2, 256)]
    public int resolution = 10;
    public bool autoUpdate = true;

    public ShapeSettings shapeSettings;
    public ColorSettings colorSettings;

    public ShapeGenerator shapeGenerator = new ShapeGenerator();
    public ColorGenerator colorGenerator = new ColorGenerator();

    [HideInInspector]
    public bool shapeSettingsFoldout;
    [HideInInspector]
    public bool colorSettingsFoldout;

    private int numberOfCubeFace = 6;

    [SerializeField, HideInInspector]
    MeshFilter[] meshFilters;
    TerrainFace[] terrainFaces;

    #endregion

    #region Method
    private void Start()
    {
        GeneratePlanet();
    }
    /// <summary>
    /// create the object planet
    /// </summary>
    void Initialize()
    {
        shapeGenerator.UpdateSettings(shapeSettings);
        colorGenerator.UpdateSettings(colorSettings);
        if (meshFilters == null || meshFilters.Length == 0)
            meshFilters = new MeshFilter[numberOfCubeFace];

        terrainFaces = new TerrainFace[numberOfCubeFace];
        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < numberOfCubeFace; i++)
        {
            if (meshFilters[i] == null)
            {
                GameObject gameObject = new GameObject("planetFace");
                //transform is the position, rotation and scale of the object
                gameObject.transform.parent = transform;

                gameObject.AddComponent<MeshRenderer>();
                meshFilters[i] = gameObject.AddComponent<MeshFilter>();
                meshFilters[i].sharedMesh = new Mesh();
            }
            meshFilters[i].GetComponent<MeshRenderer>().sharedMaterial = colorSettings.planetMaterial;

            terrainFaces[i] = new TerrainFace(shapeGenerator, meshFilters[i].sharedMesh, resolution, directions[i]);
        }
    }

    public void GeneratePlanet()
    {
        Initialize();
        GenerateMesh();
        GenerateColors();
    }

    #region update planet
    public void OnShapeSettingsUpdated()
    {
        if (autoUpdate)
        {
            Initialize();
            GenerateMesh();
        }
    }
    public void OnColourSettingsUpdated()
    {
        if (autoUpdate)
        {
            Initialize();
            GenerateColors();
        }
    }
    /// <summary>
    /// generate the face of the planet
    /// </summary>
    void GenerateMesh()
    {
        for (int i = 0; i < numberOfCubeFace; i++)
        {
            if (meshFilters[i].gameObject.activeSelf)
                terrainFaces[i].ConstructMesh();
        }    
        colorGenerator.UpdateElevation(shapeGenerator.elevationMinMax);
    }
    void GenerateColors()
    {
        colorGenerator.UpdateColors();
        for (int i = 0; i < numberOfCubeFace; i++)
        {
            if (meshFilters[i].gameObject.activeSelf)
                terrainFaces[i].UpdateUVs(colorGenerator);
        }           
    }
    #endregion


    #endregion
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeGenerator
{
    #region properties
    ShapeSettings shapeSettings;
    INoiseFilter[] noiseFilterList;
    public MinMax elevationMinMax;
    #endregion

    #region Constructor
    public void UpdateSettings(ShapeSettings shapeSettings)
    {
        this.shapeSettings = shapeSettings;
        noiseFilterList = new INoiseFilter[shapeSettings.noiseLayerList.Length];
        for (int i = 0; i < noiseFilterList.Length; i++)
            noiseFilterList[i] = NoiseFilterFactory.CreateNoiseFilter(shapeSettings.noiseLayerList[i].noiseSettings);

        elevationMinMax = new MinMax();
    }
    #endregion

    #region Method
    public float CalculateUnscaledElevation(Vector3 pointonUnitSphere)
    {
        float firstLayerValue = 0;
        float elevation = 0;

        //take account the first layer in the addition
        if (noiseFilterList.Length > 0)
        {
            firstLayerValue = noiseFilterList[0].Evaluate(pointonUnitSphere);
            if (shapeSettings.noiseLayerList[0].enabled)
                elevation = firstLayerValue;
        }

        for (int i = 1; i < noiseFilterList.Length; i++)
        {
            if (shapeSettings.noiseLayerList[i].enabled)
            {
                float mask = (shapeSettings.noiseLayerList[i].useFirstLayerAsMask) ? firstLayerValue : 1;
                elevation += noiseFilterList[i].Evaluate(pointonUnitSphere) * mask;
            }
        }
        elevationMinMax.AddValue(elevation);
        return elevation;
    }
    public float GetScaledElevation(float unscaledElevation)
    {
        float elevation = Mathf.Max(0, unscaledElevation);
        elevation = shapeSettings.planetRadius * (1 + elevation);
        return elevation;
    }
    #endregion
}

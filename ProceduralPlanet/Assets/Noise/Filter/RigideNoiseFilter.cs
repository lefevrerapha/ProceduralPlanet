using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static NoiseSettings;

public class RigideNoiseFilter : INoiseFilter
{
    #region properties
    Noise noise = new Noise();
    RigideNoiseSettings noiseSettings;
    #endregion

    #region Construcotr
    public RigideNoiseFilter(RigideNoiseSettings noiseSettings)
    {
        this.noiseSettings = noiseSettings;
    }
    #endregion

    #region Method
    public float Evaluate(Vector3 point)
    {
        float noiseValue = 0;
        float frequency = noiseSettings.baseRoughness;
        float amplitude = 1;
        float weight = 1;

        for (int i = 0; i < noiseSettings.numberLayer; i++)
        {
            float v = 1 - Mathf.Abs(noise.Evaluate(point * frequency + noiseSettings.centre));
            v *= v;
            v *= weight;
            weight = Mathf.Clamp01(v * noiseSettings.weightMultiplier);
            noiseValue += v * amplitude;

            frequency *= noiseSettings.roughness;
            amplitude *= noiseSettings.persistence;
        }
        noiseValue = noiseValue - noiseSettings.minValue;
        return noiseValue * noiseSettings.strength;
    }
    #endregion
}
